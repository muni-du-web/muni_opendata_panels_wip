<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function muni_opendata_panel_wip_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => 1);
  }
  elseif ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function muni_opendata_panel_wip_node_info() {
  $items = array(
    'fiche_donnees' => array(
      'name' => t('Fiche de données'),
      'module' => 'features',
      'description' => t('Pour les fichiers de données ouverte.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'muni' => array(
      'name' => t('Municipalité'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'has_body' => '1',
      'body_label' => t('Corps'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
