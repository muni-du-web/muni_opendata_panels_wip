<?php

/**
 * Implementation of hook_strongarm().
 */
function muni_opendata_panel_wip_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_fiche_donnees';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-3',
    'revision_information' => '0',
    'author' => '-1',
    'options' => '1',
    'menu' => '-4',
    'book' => '-2',
    'path' => '2',
    'attachments' => '4',
  );

  $export['content_extra_weights_fiche_donnees'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_muni';
  $strongarm->value = array(
    'og_user_roles_default_role' => '3',
    'title' => '-5',
    'body_field' => '-2',
    'revision_information' => '4',
    'author' => '5',
    'options' => '6',
    'menu' => '-3',
    'book' => '8',
    'path' => '7',
    'attachments' => '9',
    'og_description' => '-4',
    'og_selective' => '1',
    'og_register' => '2',
    'og_directory' => '-1',
    'og_language' => '0',
  );

  $export['content_extra_weights_muni'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_fiche_donnees';
  $strongarm->value = '0';

  $export['language_content_type_fiche_donnees'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_muni';
  $strongarm->value = '0';

  $export['language_content_type_muni'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_fiche_donnees';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );

  $export['node_options_fiche_donnees'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_muni';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_muni'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_fiche_donnees';
  $strongarm->value = '1';

  $export['upload_fiche_donnees'] = $strongarm;
  return $export;
}
