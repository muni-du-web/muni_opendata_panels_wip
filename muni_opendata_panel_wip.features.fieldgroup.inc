<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function muni_opendata_panel_wip_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_adresse
  $groups['muni-group_adresse'] = array(
    'group_type' => 'standard',
    'type_name' => 'muni',
    'group_name' => 'group_adresse',
    'label' => 'Adresse',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'cck_blocks' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '13',
    'fields' => array(
      '0' => 'field_adresse',
      '1' => 'field_email',
      '2' => 'field_siteweb',
    ),
  );

  // Exported group: group_conseil
  $groups['muni-group_conseil'] = array(
    'group_type' => 'standard',
    'type_name' => 'muni',
    'group_name' => 'group_conseil',
    'label' => 'Conseil',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'cck_blocks' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '14',
    'fields' => array(
      '0' => 'field_maire',
      '1' => 'field_conseillers',
      '2' => 'field_responsables',
      '3' => 'field_mode_election',
      '4' => 'field_prochaine_election',
      '5' => 'field_changement_regime',
      '6' => 'field_election_provinciale',
    ),
  );

  // Exported group: group_data
  $groups['muni-group_data'] = array(
    'group_type' => 'standard',
    'type_name' => 'muni',
    'group_name' => 'group_data',
    'label' => 'data',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'weight' => '29',
        'label' => 'hidden',
        'teaser' => array(
          'format' => 'simple',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'simple',
          'exclude' => 0,
        ),
        'cck_blocks' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '12',
    'fields' => array(
      '0' => 'field_specifique_toponymie',
      '1' => 'field_generique_toponymie',
      '2' => 'field_geocode',
      '3' => 'field_mrc',
    ),
  );

  // Exported group: group_information_de_base
  $groups['muni-group_information_de_base'] = array(
    'group_type' => 'standard',
    'type_name' => 'muni',
    'group_name' => 'group_information_de_base',
    'label' => 'Information de base',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'cck_blocks' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '11',
    'fields' => array(
      '0' => 'field_designation',
      '1' => 'field_gentile',
      '2' => 'field_pop',
      '3' => 'field_date_constitution',
      '4' => 'field_region_administrative',
      '5' => 'field_division_territoriale',
      '6' => 'field_recensement_canada',
      '7' => 'field_communaute_metropolitaire',
      '8' => 'field_toponymie',
      '9' => 'field_date_topo',
      '10' => 'field_source_toponymie',
      '11' => 'field_carte_topo_50',
      '12' => 'field_area',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Adresse');
  t('Conseil');
  t('Information de base');
  t('data');

  return $groups;
}
