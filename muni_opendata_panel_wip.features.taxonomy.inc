<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function muni_opendata_panel_wip_taxonomy_default_vocabularies() {
  return array(
    'thematiques_de_donnees' => array(
      'name' => 'Thématiques de données',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '1',
      'required' => '1',
      'tags' => '0',
      'module' => 'features_thematiques_de_donnees',
      'weight' => '0',
      'language' => '',
      'nodes' => array(
        'fiche_donnees' => 'fiche_donnees',
      ),
    ),
  );
}
